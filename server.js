const express = require('express');
const request = require('request');
const bodyParser = require('body-parser');
const apicache = require('apicache');

const app = express();
const cache = apicache.middleware
const port = 3001;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
apicache.options({
  appendKey: (req, res) => req.method + req.url + JSON.stringify(req.body)
})

app.post('/api/hotels', cache('1 hour'), (req, res) => {
	console.log('/api/hotels')
	request({
    url: "https://experimentation.getsnaptravel.com/interview/hotels",
    method: "POST",
    json: true,
    body: req.body
	}, function (error, response, body) {
  	res.json(body);
	});
});

app.get('/api/test', (req, res) => {
	console.log('/api/test')
  res.json({ id: 1, name: "Test"})
})

app.listen(port, () => console.log(`Listening on port ${port}`));