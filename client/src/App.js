import React, { Component } from 'react';
import ReactTable from 'react-table';
import logo from './logo.svg';
import './App.css';
import 'react-table/react-table.css';

const POST_HEADER = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    // 'Cache-Control': 'no-cache', // should cache
};

const tableColumns = [{
  Header: "Hotel Id",
  accessor: 'id'
}, {
  Header: "Hotel Name",
  accessor: 'hotel_name'
}, {
  Header: "Number of reviews",
  accessor: 'num_reviews'
}, {
  Header: "Address",
  accessor: 'address'
}, {
  Header: "Number of Stars",
  accessor: 'num_stars'
}, {
  Header: "Amenities",
  accessor: 'amenities',
  Cell: props => <ul>{props.value.map((a, i) => <li key={i}>{a}</li>)}</ul>
}, {
  Header: "Image",
  accessor: 'image_url',
  Cell: props => <img className="table-img" src={props.value}/>
}, {
  Header: "Snaptravel Price",
  accessor: 'snaptravel_price'
}, {
  Header: "Hotel.com Price",
  accessor: 'hotels_price'
}]

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      city: '',
      checkin: '',
      checkout: '',
      hotels: []
    }
  }

  submitForm() {
    const {city, checkin, checkout} = this.state;
    if(city === "" || checkin === "" || checkout === "") {
      return
    }
    const dataURL = '/api/hotels';
    const snaptravelData = {
      city,
      checkin,
      checkout,
      provider: 'snaptravel'
    };
    const hotelsData = {
      city,
      checkin,
      checkout,
      provider: 'retail'
    };

    const snaptravelRequest = fetch(dataURL, {
      headers: POST_HEADER,
      method: 'post',
      body: JSON.stringify(snaptravelData)
    }).then(res => res.json());

    const hotelsRequest = fetch(dataURL, {
      headers: POST_HEADER,
      method: 'post',
      body: JSON.stringify(hotelsData)
    }).then(res => res.json());  

    Promise.all([snaptravelRequest, hotelsRequest]).then(values => {
      this.renderTable(values[0].hotels, values[1].hotels);
    })  
  }

  renderTable(snaptravelRates, hotelsRates) {
    const intersectionRates = [];
    snaptravelRates.forEach(snaptravelRate => {
      hotelsRates.forEach(hotelsRate => {
        if(snaptravelRate.id === hotelsRate.id) {
          intersectionRates.push({
            id: snaptravelRate.id,
            address: snaptravelRate.address,
            amenities: snaptravelRate.amenities,
            hotel_name: snaptravelRate.hotel_name,
            image_url: snaptravelRate.image_url,
            num_reviews: snaptravelRate.num_reviews,
            num_stars: snaptravelRate.num_stars,
            snaptravel_price: snaptravelRate.price,
            hotels_price: hotelsRate.price
          })
        }
      })
    });
    this.setState({hotels: intersectionRates})
  }

  render() {
    let hotelsTable;
    const {hotels} = this.state;
    if(hotels.length > 0) {
      hotelsTable = (
        <div className="table">
          <ReactTable
            columns={tableColumns}
            data={hotels}
            minWidth={50}
          />
        </div>
      );
    }

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">snaptravel Demo</h1>
        </header>
        <div className="form">
          <input
            className='form-input'
            type='text'
            placeholder='City'
            value={this.state.city}
            onChange={(e) => this.setState({city: e.target.value})}
          />
          <input
            className='form-input'
            type='text'
            placeholder='Checkin'
            value={this.state.checkin}
            onChange={(e) => this.setState({checkin: e.target.value})}
          />
          <input
            className='form-input'
            type='text'
            placeholder='Checkout'
            value={this.state.checkout}
            onChange={(e) => this.setState({checkout: e.target.value})}
          />
          <div 
            className="form-submit"
            onClick={() => this.submitForm()}
          >
            Submit
          </div>
        </div>
        {hotelsTable}
      </div>
    );
  }
}

export default App;
